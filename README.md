
 We have all been there. Wanting to work out but is too lazy to go to the gym. Well look no more, Letz Get It is a fitness app for those who can’t make it to the gym every now and then or even for those who are paying too much for their memberships to keep their body right. Now is the time to ditch your old gym membership and join Letz get it.

Letz get it is an app anyone can use starting at ages 8+. Letz get it was designed to keep you fit and healthy. Letz Get it is a fitness online/mobile app that does it all. We allow customers to watch our workout sessions, buy protein shakes, equipment, merchandise, etc. The aim of this app is to make your lifestyle healthier by tracking your eating and workout pattern


##### Breif Summary

<details>
    <br>
Metrid Okumu and I are collaberating on a unique fitness app. We have all been there. Wanting to work out but is too lazy to go to the gym. Well look no more, Letz Get It is a fitness app for those who can’t make it to the gym every now and then or even for those who are paying too much for their memberships to keep their body right. Now is the time to ditch your old gym membership and join Letz get it.

Letz get it is an app anyone can use starting at ages 8+. Letz get it was designed to keep you fit and healthy. Letz Get it is a fitness online/mobile app that does it all. We allow customers to watch our workout sessions, buy protein shakes, equipment, merchandise, etc. The aim of this app is to make your lifestyle healthier by tracking your eating and workout pattern
</details>

##### One Pager
<details>
    <br>
Introduction: We have all been there. Wanting to work out but is too lazy to go to the gym. Well look no more, Letz Get It is a fitness app for those who can’t make it to the gym every now and then or even for those who are paying too much for their memberships to keep their body right. Now is the time to ditch your old gym membership and join Letz get it.


Brief Description: Letz Get It is an app anyone can use starting at ages 8+. Letz Get It was designed to keep you fit and healthy. Letz Get it is a fitness online/mobile app that does it all. We allow customers to watch our workout sessions, buy protein shakes, equipment, and merchandise and provide a customized meal plan and recipes among others. This app aims to help users adopt a healthy lifestyle by tracking their eating and workout pattern.

Problem Statement: Most people struggle with finding fitness exercises and meal plans that'll help them lead to healthier lifestyles. This is because they do not understand their body type and its metabolism. Letz Get It customizes exercises and suggests healthy meal plans for its users to achieve the best results. 

Desired Outcome: For the exact purpose of Letz Get It is to have the best work out of your life. This is by breaking down how to do exercises correctly and other healthy meal options, all at home at the fraction of the cost for anyone who registers. We want customers to recommend our app so we can get fit together.

Potential Competition: There are other gyms such as planet fitness and apps that may offer the same essentials but, Letz get it will have a variety of exercises like heavy lifting, stretching, running, and ab workout, and further we will customize our workouts to every customer. This will ensure optimal results within a reasonable period.

</details>

##### Feature List
<details>
    <br>

1.	User username & password
User story: As a customer, I want the ability to log in to my account easily and safely so that no one can buy items on my account without my approval.

2.	Push notifications 
User Story: As a customer, I want to get notifications so that I know what exercises to do for the day.

3.	Personalized home screen
User story: as a customer having a personalized home screen helps weed out workouts that I am not interested in, so I can look at what I want to exercise.

4.	Image and uploads
User story: As a customer, I want to be able to see other varieties of workout exercises to get an understanding of how to do them.

5.	Payment Option
User story: As a customer, I don’t bring my cards with me so I don’t lose them, I would like to have the option to pay for products in my account and be able to store payment.

6.	Contact
User story: AS a customer I want to be able to ask questions or have some way of getting     in touch with the owner or worker of the establishment.

7.	Profile 
User story: As a customer, I want to be able to access my information with ease so that I can access and edit the information that is displayed.

8.	Review/feedback 
User story: As a customer, I want to give my views so that positive changes may be made.

9.	Community 
User story: As a customer, I want to be able to connect with other people using the app so that we can build a fitness community.

10.	Shopping
User story: As a customer, I want to be able to purchase merchandise from the establishment that is displayed for sale.

11.	Meal plans 
User story: As a customer, I want to track my calorie intake so that I can meet calorie intake goals.

12.	Progress chart/report   
User story: As a customer, I want to get progress updates so that I know when I reach my goal.

</details>

